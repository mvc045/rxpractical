//
//  AppCoordinator.swift
//  RxPractical
//
//  Created by mvc on 15.04.2022.
//

import Foundation
import UIKit

class AppCoordinator {

    private let window: UIWindow
    
    init(window: UIWindow) {
        self.window = window
    }
    
    func start() {
        let vc = ViewController()
        vc.viewModel = PostsViewModel()
        window.rootViewController = UINavigationController(rootViewController: vc)
        window.makeKeyAndVisible()
    }
    
}

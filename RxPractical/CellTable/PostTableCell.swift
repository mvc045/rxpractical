//
//  PostTableCell.swift
//  RxPractical
//
//  Created by mvc on 15.04.2022.
//

import UIKit

class PostTableCell: UITableViewCell {

    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var noteLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        mainView.layer.cornerRadius = 12
    }
    
    func setup(_ postModel: PostViewModel) {
        titleLabel.text = postModel.title
        noteLabel.text = postModel.note
    }
    
}

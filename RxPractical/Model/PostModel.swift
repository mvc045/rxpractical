//
//  PostModel.swift
//  RxPractical
//
//  Created by mvc on 15.04.2022.
//

import Foundation

struct Post: Decodable {
    var userId: Int
    var id: Int
    var title: String
    var body: String
}


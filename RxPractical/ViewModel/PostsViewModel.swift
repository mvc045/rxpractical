//
//  PostsViewModel.swift
//  RxPractical
//
//  Created by mvc on 15.04.2022.
//

import Foundation
import RxSwift

final class PostsViewModel {
    
    let title = "Posts"
    
    private let postsService: PostsServiceProtocol
    
    var isLoading = PublishSubject<Bool>()
    
    
    init(_ service: PostsServiceProtocol = PostsService()) {
        postsService = service
        
    }

    func fetchPostsViewModels() -> Observable<[PostViewModel]> {
        self.isLoading.onNext(true)
        return postsService
            .fetchPosts()
            .map({ item in
                self.isLoading.onNext(false)
                return item.map({ PostViewModel($0)})
            })
    }
    
}

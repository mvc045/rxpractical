//
//  PostViewModel.swift
//  RxPractical
//
//  Created by mvc on 15.04.2022.
//

import Foundation

struct PostViewModel {
    
    private var post: Post
    
    var title: String {
        return post.title
    }
    var note: String {
        return post.body
    }
    
    init(_ post: Post) {
        self.post = post
    }
    
}

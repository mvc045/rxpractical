//
//  ViewController.swift
//  RxPractical
//
//  Created by mvc on 15.04.2022.
//

import UIKit
import RxSwift
import RxCocoa

class ViewController: UIViewController {
    
    let disposeBag = DisposeBag()
    let service = PostsService()
    var viewModel: PostsViewModel! 
    
    var mTableView: UITableView = {
        let table = UITableView()
        table.separatorStyle = .none
        table.backgroundColor = .systemGray6
        table.translatesAutoresizingMaskIntoConstraints = false
        table.register(UINib(nibName: "PostTableCell", bundle: nil), forCellReuseIdentifier: "Cell")
        return table
    }()
    
    var mSpinner: UIActivityIndicatorView = {
        let spinner = UIActivityIndicatorView(style: .large)
        spinner.translatesAutoresizingMaskIntoConstraints = false
        spinner.hidesWhenStopped = true
        return spinner
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = viewModel.title
        
        setupTable()
        setupSpinner()
        
        loadData()
    }
    
    func loadData() {
        viewModel.fetchPostsViewModels()
            .bind(to: mTableView.rx.items(cellIdentifier: "Cell", cellType: PostTableCell.self)) { index, viewModel, cell in
                cell.setup(viewModel)
            }
            .disposed(by: disposeBag)
    }
    
    func setupTable() {
        view.addSubview(mTableView)
        mTableView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        mTableView.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        mTableView.widthAnchor.constraint(equalToConstant: UIScreen.main.bounds.width).isActive = true
        mTableView.heightAnchor.constraint(equalToConstant: UIScreen.main.bounds.height).isActive = true
    }
    
    func setupSpinner() {
        view.addSubview(mSpinner)
        mSpinner.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        mSpinner.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        mSpinner.widthAnchor.constraint(equalToConstant: 100).isActive = true
        mSpinner.heightAnchor.constraint(equalToConstant: 100).isActive = true
        
        viewModel.isLoading
            .subscribe(onNext: { [weak self] status in
                if status {
                    self?.mSpinner.startAnimating()
                } else {
                    self?.mSpinner.stopAnimating()
                }
            })
            .disposed(by: disposeBag)
    }

}


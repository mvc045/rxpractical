//
//  PostsProtocol.swift
//  RxPractical
//
//  Created by mvc on 15.04.2022.
//

import Foundation
import RxSwift

protocol PostsServiceProtocol {
    
    func fetchPosts() -> Observable<[Post]>
    
}

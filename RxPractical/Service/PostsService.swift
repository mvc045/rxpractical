//
//  PostsService.swift
//  RxPractical
//
//  Created by mvc on 15.04.2022.
//

import Foundation
import RxSwift

// Данные для теста
// GET | http://jsonplaceholder.typicode.com/posts

class PostsService: PostsServiceProtocol {
    
    // Фейковый запрос на сервер
    func fetchPosts() -> Observable<[Post]> {
        return Observable.create { observer -> Disposable in
            guard let path = Bundle.main.path(forResource: "posts", ofType: "json") else {
                observer.onError(NSError(domain: "Не смогли открыть файл", code: -1, userInfo: nil))
                return Disposables.create {  }
            }
            do {
                let dataFile = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let posts = try JSONDecoder().decode([Post].self, from: dataFile)
                
                // Задержка ответа 5 сек.
                DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
                    observer.onNext(posts)
                }
            } catch {
                observer.onError(error)
            }
            return Disposables.create { }
        }
    }
    
}
